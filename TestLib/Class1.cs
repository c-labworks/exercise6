﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestLib
{
    public class Class1
    {
        public Class1() { }

        public int Age { get; }

        private string m_sName;

        public string Name
        {
            get { return m_sName; }
            set { m_sName = value; }
        }

        public double Square(double val)
        {
            return val * val;
        }

        public string Hello()
        {
            return "Hello";
        }

        public int Count;
        public static string StrField;
    }
}

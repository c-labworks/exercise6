﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssemblerLoader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private Assembly a = null;

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
                return;

            tbFileName.Text = openFileDialog1.FileName;

            try
            {
                a = Assembly.LoadFrom(tbFileName.Text);

                foreach (Type t in a.GetTypes())
                {
                    lb.Items.Add("");
                    lb.Items.Add(t.Name);

                    // MemberInfo[] mis = t.GetMembers();
                    // foreach (MemberInfo mi in mis)
                    // {
                    //     lb.Items.Add("--- " + mi.Name);
                    // }

                    PropertyInfo[] pis = t.GetProperties();
                    lb.Items.Add("---properties---");
                    foreach (PropertyInfo p in pis)
                    {
                        string r = p.CanRead ? "get; " : "";
                        string w = p.CanWrite ? "set; " : "";

                        lb.Items.Add("    " + p.PropertyType.Name + " " + p.Name + "{ " + r + w + " }");
                    }

                    lb.Items.Add("---Methods---");
                    MethodInfo[] mis = t.GetMethods();
                    foreach (MethodInfo m in mis)
                    {
                        string s = m.IsStatic ? "static " : "";

                        s += " " + m.Name + "(";
                        
                        foreach (ParameterInfo pi in m.GetParameters())
                        {
                            s += (pi.ParameterType + " " + pi.Name + ", ");
                        }

                        s += ")";

                        lb.Items.Add("    " + s);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка загрузки библиотеки!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string res = null;

            string methodStr = methodTxt.Text;

            int endOfClassIdx = methodStr.IndexOf(".", StringComparison.Ordinal);
            int endOfMethodIdx = methodStr.IndexOf("(", StringComparison.Ordinal);
            int endOfPramsIdx = methodStr.IndexOf(")", StringComparison.Ordinal);

            string className = "";
            string methodName = "";
            string paramsString = "";
            string exceptionMessage = null;
            Type returnType = null;

            if (a == null)
            {
                exceptionMessage = "Не выбран файл!";
            }

            try
            {
                className = methodStr.Substring(0, endOfClassIdx);
                methodName = methodStr.Substring(endOfClassIdx + 1, endOfMethodIdx - endOfClassIdx - 1);
                paramsString = methodStr.Substring(endOfMethodIdx + 1, endOfPramsIdx - endOfMethodIdx - 1)
                    .Replace(" ", "");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                exceptionMessage = exception.Message;
            }


            if (exceptionMessage == null)
            {
                Type typeClass = getTypeClass(className);
                if (typeClass != null)
                {
                    object obj = Activator.CreateInstance(typeClass);
                    MethodInfo methodHello = typeClass.GetMethod(methodName);
                    try
                    {
                        object[] paramArr = parseParams(methodHello, paramsString);
                        if (methodHello.ReturnType == typeof(void))
                        {
                            res = "ok";
                        }
                        else
                        {
                            Object reso = methodHello.Invoke(obj, paramArr);
                            res = reso == null ? "null" : reso.ToString();
                        }
                    }
                    catch (NullReferenceException nre)
                    {
                        exceptionMessage = "Метод не найден!";
                        Console.WriteLine(nre.Message);
                        Console.WriteLine(nre.StackTrace);
                    }
                }
                else
                {
                    exceptionMessage = "Класс не найден!";
                }
            }

            if (exceptionMessage == null)
            {
                execResult.Items.Add("Results: " + res);
            }
            else
            {
                MessageBox.Show(exceptionMessage, "Ошибка обработки!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Type getTypeClass(string className)
        {
            var fullName = "TestLib." + className;
            return a.GetTypes().FirstOrDefault(type => type.FullName.Equals(fullName));
        }

        private object[] parseParams(MethodInfo methodInfo, string paramsStr)
        {
            List<object> objectList = new List<object>();

            string[] paramStrArr = paramsStr.Length > 0
                ? paramsStr.Split(Convert.ToChar(","))
                : new string[] { };

            for (int i = 0; i < methodInfo.GetParameters().Length; i++)
            {
                objectList.Add(Convert.ChangeType(paramStrArr[i], methodInfo.GetParameters()[i].ParameterType));
            }

            return objectList.ToArray();
        }
    }
}
﻿namespace AssemblerLoader
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.lb = new System.Windows.Forms.ListBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.callMethodTxt = new System.Windows.Forms.Button();
            this.methodTxt = new System.Windows.Forms.TextBox();
            this.execResult = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(82, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Выбрать .dll";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbFileName
            // 
            this.tbFileName.Location = new System.Drawing.Point(110, 15);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.Size = new System.Drawing.Size(495, 20);
            this.tbFileName.TabIndex = 1;
            // 
            // lb
            // 
            this.lb.FormattingEnabled = true;
            this.lb.Location = new System.Drawing.Point(12, 54);
            this.lb.Name = "lb";
            this.lb.Size = new System.Drawing.Size(597, 121);
            this.lb.TabIndex = 2;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(10, 185);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 29);
            this.label1.TabIndex = 3;
            this.label1.Text = "Введите команду в виде:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(153, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(334, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Имя_Тип.Имя_Метода(Знач_арг1, Знач_арг2, ..., Знач_аргZ)";
            // 
            // callMethodTxt
            // 
            this.callMethodTxt.Location = new System.Drawing.Point(17, 224);
            this.callMethodTxt.Name = "callMethodTxt";
            this.callMethodTxt.Size = new System.Drawing.Size(77, 25);
            this.callMethodTxt.TabIndex = 5;
            this.callMethodTxt.Text = "Ввести";
            this.callMethodTxt.UseVisualStyleBackColor = true;
            this.callMethodTxt.Click += new System.EventHandler(this.button2_Click);
            // 
            // methodTxt
            // 
            this.methodTxt.Location = new System.Drawing.Point(117, 226);
            this.methodTxt.Name = "methodTxt";
            this.methodTxt.Size = new System.Drawing.Size(487, 20);
            this.methodTxt.TabIndex = 6;
            // 
            // execResult
            // 
            this.execResult.FormattingEnabled = true;
            this.execResult.Location = new System.Drawing.Point(8, 317);
            this.execResult.Name = "execResult";
            this.execResult.Size = new System.Drawing.Size(597, 43);
            this.execResult.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 404);
            this.Controls.Add(this.execResult);
            this.Controls.Add(this.methodTxt);
            this.Controls.Add(this.callMethodTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lb);
            this.Controls.Add(this.tbFileName);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button callMethodTxt;
        private System.Windows.Forms.ListBox execResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lb;
        private System.Windows.Forms.TextBox methodTxt;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox tbFileName;

        #endregion
    }
}


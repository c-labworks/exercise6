﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Reflection212
{
    class Program
    {
        static void Main(string[] args)
        {
            Type typeClass;
            //Рефлексии
            //1
            //typeClass = typeof(TestClass);

            //2
            //TestClass testClass=new TestClass();
            //typeClass = testClass.GetType();

            //3
            typeClass = Type.GetType("Reflection212.TestClass");

            MemberInfo[] mi = typeClass.GetMembers();
            Console.WriteLine("---Members---");
            foreach (MemberInfo m in mi)
            {
                Console.WriteLine(m.Name);
            }

            PropertyInfo[] pis = typeClass.GetProperties();
            Console.WriteLine("---properties---");
            foreach (PropertyInfo p in pis)
            {
                string r = p.CanRead ? "get; " : "";
                string w = p.CanWrite ? "set; " : "";
                Console.WriteLine(p.PropertyType.Name + " "
                            + p.Name + "{ "
                            + r + w + " }");
            }

            Console.WriteLine("---Fields---");
            FieldInfo[] fis = typeClass.GetFields();
            foreach (FieldInfo f in fis)
            {
                string s = f.IsStatic ? "static " : "";
                Console.WriteLine(
                            s + " "
                            + f.FieldType.Name + " "
                            + f.Name);
            }

            Console.WriteLine("---Methods---");
            MethodInfo[] mis = typeClass.GetMethods();
            foreach (MethodInfo m in mis)
            {
                string s = m.IsStatic ? "static " : "";
                Console.Write(
                            s + " "
                            + m.Name + "(");
                foreach (ParameterInfo pi in m.GetParameters())
                {
                    Console.Write(pi.ParameterType + " "
                                + pi.Name + ", ");
                }
                Console.WriteLine(")");
            }

            //Позднее связывание
            Console.WriteLine("---Позднее связывание---");
            object obj = Activator.CreateInstance(typeClass);
            MethodInfo methodHello = typeClass.GetMethod("Hello");
            string res = methodHello.Invoke(obj, new object[] { }).ToString();
            Console.WriteLine(res);

            MethodInfo methodSq = typeClass.GetMethod("Square");
            res = methodSq.Invoke(obj, new object[] { 4 }).ToString();
            Console.WriteLine(res);

            Console.ReadKey();
        }
    }
}
